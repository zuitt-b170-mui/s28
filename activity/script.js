fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json[0]));

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));

// POST
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        userId: 1,
        id: 201,
        title: "hello world",
        completed: true
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        title: "hello world",
        description: "Updated Post",
        status: "completed",
        dateCompleted: "today",
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        userId: 99,
        id: 210,
        title: "update PATCH",
        completed: true
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method: "DELETE"
});