console.log("Hello World");
// console.log("Hello Again");
console.log("Goodbye");

console.log("Hello World");
for(let i = 0; i<= 10; i++){
    // console.log(i);
};
console.log("Hello Again");


// FETCH function Fetch API
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

console.log("Hello Again");

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json));

async function fetchData(){
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")
    console.log(result);
    console.log(typeof result);
    console.log(result.body);
    let json = await result.json();
    console.log(json);
}
fetchData();
console.log("Hello Again");

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((json) => console.log(json[0]));

fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        userId: 1,
        title: "New Post",
        body:"Hello World"
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// mini activity
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        title: "Corrected Post",
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// mini activity
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PATCH",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        userId: 1,
        title: "Updated Post",
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "DELETE"
});

fetch("https://jsonplaceholder.typicode.com/posts?userId=1&id=3")
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));

